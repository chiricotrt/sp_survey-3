# Forces all threads to share the same connection. This works on
# Capybara because it starts the web server in a thread.

class ActiveRecord::Base
  mattr_accessor :shared_connections
  self.shared_connections = {}

  # def self.connection
  #   @@shared_connection || retrieve_connection
  # end
  def self.connection
    shared_connections[connection_config[:database]] ||= begin
      retrieve_connection
    end
  end
end

# ActiveRecord::Base.shared_connection = ActiveRecord::Base.connection

module MutexLockedQuerying
  @@semaphore = Mutex.new

  def query(*)
    @@semaphore.synchronize { super }
  end
end

Mysql2::Client.send(:prepend, MutexLockedQuerying)
