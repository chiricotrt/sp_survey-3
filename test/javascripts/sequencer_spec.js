module("Sequencer");

asyncTest('Check normal run', 1, function() {

	var demo = new smart.Sequencer();
	demo.registerStep(new smart.SequenceStep(function(step){ console.log("step1"); jQuery.publish(step._stepId,[{step1: true}]); }, function(){} ));
	demo.registerStep(new smart.SequenceStep(function(step){ console.log("step2"); jQuery.publish(step._stepId,[{step2: true}]); }, function(){}));
	demo.registerAfterHook(function(data){
		console.log(data);
		deepEqual(data, {step1: true, step2: true}, "Output matches");
		start();
	});

	demo.run();
});