// JavaScript Document

var ContentHeight =475;

var TimeToSlide = 500.0;

var openAccordion = '';

function runAccordion(index, activity_type)
{
  var accordionWasOpened = false;
  var nID = "Accordion" + index + "Content";
  var index_str = '' + index;

  // console.log("open", openAccordion, "index", index, nID, openAccordion == index_str);

  if(openAccordion == index_str){
    jQuery('#'+nID).slideUp();
    var current_button = jQuery('#'+index + ' .btn-edit');
    current_button.text(get_text_to_action_button(index));
    index = '';
    accordionWasOpened = false;

  } else {
    if (openAccordion === '') {
      var coord_ele = jQuery(('#'+index));
      var pageCoord = coord_ele.offset();

      if (pageCoord !== null){
        jQuery('body').animate({ scrollTop: pageCoord.top-86});
      }
    } else {
      jQuery('#Accordion'+openAccordion+'Content').slideUp();
      current_button = jQuery('#'+openAccordion + ' .btn-edit');
      current_button.text(get_text_to_action_button(openAccordion));
    }
    jQuery('#'+nID).slideDown({ complete: function(){
                                              var coord_ele = jQuery(('#'+index));
                                              var pageCoord = coord_ele.offset();
                                              // console.log("pageCoord", coord_ele, pageCoord);

                                              if (pageCoord !== null) {
                                                jQuery('body').animate({ scrollTop: pageCoord.top-86});
                                              }
                                            }
                              });
    jQuery('#'+index + ' .btn-edit').text(I18n.t("generic.close"));
    accordionWasOpened = true;
  }
  window.openStop = index;
  openAccordion = index;

  if (index.length === 0) {
    index = 'Travel0';
  }
  jQuery.publish("ui:open_row", [index, activity_type, nID]);
  // if (accordionWasOpened){
  //   jQuery.publish("ui:open_row", [ whichAccordionIsOpen(), isRowTravel() ]);
  // } else {
  //   jQuery.publish("ui:close_row", [ whichAccordionIsOpen(), isRowTravel() ]);
  // }
}

function get_text_to_action_button(id){
  return isStopVerified(id) ? I18n.t("report.verified") : I18n.t("report.verify");
}

function updateAccordion(index){
  var nID = "Accordion" + index + "Content";

  //var elm = document.getElementById(nID)
  //jQuery('#'+nID).animate({ height: GetHeight(elm)+10 }, 500);
}

function GetHeight(elm)
{
    var oDiv = elm;
    var sOriginalOverflow = oDiv.style.overflow;
    var sOriginalHeight = oDiv.style.height;
    oDiv.style.overflow = "";
    oDiv.style.height = "";
    var height = oDiv.offsetHeight;
    oDiv.style.height = sOriginalHeight;
    oDiv.style.overflow = sOriginalOverflow;
    return height;
}

function whichAccordionIsOpen(){
  var result = (""+openAccordion).match(/[\d]+/);
  if (result!== null){
    result = result[0];
  }
	return result;
}

function isRowTravel(id){
  var result;
  if (id == null) {
    result = (""+openAccordion).match(/Travel/i);
  } else {
    result = (""+id).match(/Travel/i);
  }

  if (result != null) {
    return true;
  } else {
    return false;
  }
}

function hasAccordionOpen(){
	return (openAccordion != '');
}

function resetAccordionOpen(){
	openAccordion = '';
}


function showWaitIndicator() { jQuery("div.wait_indicator").show(); }
function hideWaitIndicator() { jQuery("div.wait_indicator").hide(); }
