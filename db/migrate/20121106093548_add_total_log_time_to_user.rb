class AddTotalLogTimeToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :totalLogTime, :integer
  end

  def self.down
    remove_column :users, :totalLogTime
  end
end
