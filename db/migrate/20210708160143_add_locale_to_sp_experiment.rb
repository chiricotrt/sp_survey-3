class AddLocaleToSpExperiment < ActiveRecord::Migration
  def change
    add_column :sp_experiments, :locale, :string
  end
end

