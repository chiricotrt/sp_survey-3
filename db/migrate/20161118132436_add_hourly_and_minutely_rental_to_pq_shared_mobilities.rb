class AddHourlyAndMinutelyRentalToPqSharedMobilities < ActiveRecord::Migration
  def change
    add_column :pq_shared_mobilities, :monthly_rental_hours, :integer, default: 0
    add_column :pq_shared_mobilities, :monthly_rental_minutes, :integer, default: 0
  end
end
