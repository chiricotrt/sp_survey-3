class AddCompletionTimesToSpExperiments < ActiveRecord::Migration
  def change
    add_column :sp_experiments, :completion_times, :text
  end
end
